(ns qbf.write-actions-test
  (:require [clojure.test :refer :all]
            [qbf.model :refer :all]
            [qbf.write-actions :refer :all])
  (:import [qbf.model default-act string-act macro-act]))

(deftest a-test
  (testing "writing default action"
    (is (= (write (->default-act "a" "KEY_A")) "default a KEY_A")))
  (testing "writing macro action"
    (is (= (write (->macro-act "a" ["b" "c" "d"] 0)) "Action * a_macro[3] = {&b, &c, &d};\nMacroAction(a_macro, 3, 0);")))
  (testing "writing vector of actions"
    (is (= (write-actions [(->default-act "a" "KEY_A") (->default-act "b" "KEY_B")]) "default a KEY_A\ndefault b KEY_B"))))