(ns qbf.write-test
  (:require [clojure.test :refer :all]
            [qbf.model :refer :all]
            [qbf.write :refer :all])
  (:import [qbf.model default-act string-act]))

(deftest a-test
  (testing "writing default action"
    (is (= (write (->default-act "a" "KEY_A")) "default a KEY_A")))
  (testing "writing vector of actions"
    (is (= (write-actions [(->default-act "a" "KEY_A") (->default-act "b" "KEY_B")]) "default a KEY_A\ndefault b KEY_B"))))