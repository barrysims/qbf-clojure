(ns qbf.model-test
  (:require [clojure.test :refer :all]
            [qbf.model :refer :all]))

(deftest a-test
  (testing "max-point correctly gets highest val"
  	(is (= (max-point {[0 0] [0 0], [0 1] [1 1]}) 1)))

  (testing "remap-layer correctly maps a physical layout to a matrix layout"
  	(def layer {[0 0] "a", [0 1] "b"})
  	(def mapping {[0 0] [0 0], [0 1] [1 1]})
    (is (= (remap-layer layer mapping) ""))))