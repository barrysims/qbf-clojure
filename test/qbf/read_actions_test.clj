(ns qbf.read-actions-test
  (:require [clojure.test :refer :all]
            [qbf.read-actions :refer :all]
            [qbf.model :refer :all]
            [the.parsatron :refer :all]
            [clojure.java.io :as io])
  (:import [qbf.model default-act string-act]))

(def data-file (io/file (io/resource "actions.txt" )))

(deftest a-test
  (testing "parsing default action"
    (is (= (run (default-action-parser) "default a KEY_A") (->default-act "a" "KEY_A"))))
  (testing "parsing actions file"
    (is (= (run (action-parser) (slurp data-file))
          [(->default-act "a" "KEY_A") (->default-act "b" "KEY_B") (->default-act "c" "KEY_C")]))))