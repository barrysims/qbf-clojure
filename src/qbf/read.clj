(ns qbf.read
  (:refer-clojure :exclude [char])
  (:require [the.parsatron :refer :all])
  (:require [qbf.model :refer :all]))

(defn chars-to-string
  "make a string from a sequence of characters"
  [chars]
  (apply str chars))

(defparser spc []
  (many (token #{\space \tab \newline})))

(defparser word []
  (let->> [chars (many1 (either (letter) (char \_)))]
    (always (chars-to-string chars))))

(defparser hashed []
  (let->> [unhashed (many1 (letter))]
    (always (chars-to-string unhashed))))

(defparser default-action-parser []
  (let->> [_ (string "default") _ (spc) name (hashed) _ (spc) value (word) _ (spc)]
    (always (->default-act name value))))

(defparser string-action-parser []
  (let->> [_ (string "string") _ (spc) name (word) _ (spc) value (word) _ (spc)]
    (always (->string-act name value))))

(defparser action-parser []
  (let->> [acts (many (choice (default-action-parser) (string-action-parser))) _ (eof)]
    (always acts)))