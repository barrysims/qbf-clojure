(ns qbf.write
  (:require [hbs.core :refer :all])
  (:use [hbs core helper])
  (:require [qbf.model :refer :all])
  (:import [qbf.model default-act string-act]))

(set-template-path! "/templates" ".hb")

(defprotocol Write
  (write [val]))

(extend-type default-act
  Write
  (write [da] (apply str ["default " (:name da) " " (:action da)])))

(extend-type string-act
  Write
  (write [sa] (apply str ["string " (:name sa) " " (:action sa)])))

(defn write-actions [as]
  (reduce str (interpose "\n" (map write as))))

