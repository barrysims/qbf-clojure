(ns qbf.core
  (:use qbf.model)
  (:use qbf.read-actions)
  (:use qbf.write-actions)
  (:use [the.parsatron])
  (:require [clojure.java.io :as io]))

(def data-file (io/file (io/resource "hello.txt" )))

(defn -main []
  (println (slurp data-file))
  (def action (->default-act "hello" "world"))
  (println action)
  (println (write action))
  (println (run (default-action-parser) "default hello world")))
