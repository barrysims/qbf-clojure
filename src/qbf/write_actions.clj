(ns qbf.write-actions
  (:require [hbs.core :refer :all])
  (:use [hbs core helper])
  (:require [qbf.model :refer :all])
  (:import [qbf.model default-act string-act modifier-act toggle-act dual-act macro-act]))

(set-template-path! "/templates" ".hb")

(defn- simple-action [type a]
  (apply str [type " " (:name a) " " (:action a)]))

(defprotocol Write
  (write [val]))

(extend-type default-act
  Write (write [a] (simple-action "default" a)))

(extend-type modifier-act
  Write (write [a] (simple-action "modifier" a)))

(extend-type string-act
  Write (write [a] (simple-action "string" a)))

(extend-type toggle-act
  Write (write [a] (simple-action "toggle" a)))

(extend-type dual-act
  Write (write [a] (apply str ["dual " (:name a) " " (:hold-action a) " " (:release-action a)])))

(extend-type macro-act
  Write
  (write [a]
    (def array (reduce str (interpose ", " (map (fn [s] (apply str["&" s])) (:action-list a)))))
    (def array-stmt (apply str ["Action * " (:name a) "_macro[" (count (:action-list a)) "] = {" array "};"]))
    (def macro-stmt (apply str ["MacroAction(" (:name a) "_macro, " (count (:action-list a)) ", " (:clear a) ");"]))
    (apply str [array-stmt "\n" macro-stmt])))

(defn write-actions [as]
  (reduce str (interpose "\n" (map write as))))

