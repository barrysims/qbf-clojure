(ns qbf.model)

(defrecord default-act [name action])
(defrecord modifier-act [name action])
(defrecord string-act [name action])
(defrecord dual-act [name hold-action release-action])
(defrecord toggle-act [name action])
(defrecord macro-act [name action-list clear])

(defrecord teensy-pin [number axis])
(defrecord expander-pin [number axis])

(defrecord point [col row])
(defrecord layout [layers mapping])

;(defn createMapping [layers]
;  (def maximum (max ())))

(defn remap-layer
  "Expects:
   layer in the form {[0 0] \"a\", [0 1] \"b\"}
   mapping in the form {[0 0] [0 0], [0 1] [1 1]}"
  [layer mapping]
  (for [c (range 0 (:cols mapping))
  		r (range 0 (:rows mapping))]
  	[(get (:point-matrix-map mapping) (->point c r))]))

(defn fst [vect] (nth vect 0))

(defn max-point [mapping]
	(apply max (map (comp fst fst) mapping)))