(defproject qbf "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [the/parsatron "0.0.7"]
                 [de.ubercode.clostache/clostache "1.4.0"]
                 [hbs "0.5.1"]]
  :main ^:skip-aot qbf.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-marginalia "0.8.0"]])